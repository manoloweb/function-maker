from dotenv import load_dotenv, find_dotenv
import os
import openai
import argparse
import sys


load_dotenv(find_dotenv())

openai.api_key = os.getenv("OPENAI_API_KEY")

parser = argparse.ArgumentParser(description='Create a function from a prompt')
parser.add_argument('-f', '--file', nargs='?', default='-', help='File to read the prompt from, defaults to stdin')
parser.add_argument('-l', '--language', type=str, default='python',help='Language to use to create the function')

args = parser.parse_args()

# Check if the file is a file or stdin, then validate if the file exists
if args.file == '-':
    prompt = sys.stdin.read()
else:
    if os.path.isfile(args.file):
        with open(args.file, 'r') as f:
            prompt = f.read()
    else:
        print(f'File {args.file} does not exist')
        sys.exit(1)

prompt = f'Act as a professional developer. Do not return any additional text or explanations, only code. Create a function in {args.language} that does the following: {prompt}'

# Create the function using openai
response = openai.ChatCompletion.create(
    model="gpt-3.5-turbo",
    messages=[{"role": "user", "content": prompt}],
    temperature=0,
    max_tokens=1000
)

# Print the function
print(response.choices[0].message.content)