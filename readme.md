# Function Maker

## Description

This project is designed to generate a function from a prompt using the OpenAI API. The generated function will perform the desired task based on the provided prompt. The code provided in the `make_function.py` file utilizes the OpenAI Chat Completion API to create the function.

## Prerequisites

Before running the code, make sure you have the following:

- Python (version 3.7 or above)
- An OpenAI API key

## Installation

1. Clone the repository or download the project files.

2. Install the required Python packages by running the following command:

   ```
   pip install -r requirements.txt
   ```

## Getting an OpenAI API Key

To use the OpenAI API, you need an API key. Here's how you can get one:

1. Visit the [OpenAI website](https://platform.openai.com/) and sign in to your account or create a new one if you don't have an account yet.

2. Once you're logged in, navigate to the API section or visit the [OpenAI API Key Page](https://platform.openai.com/account/api-keys) directly.

3. Follow the instructions provided by OpenAI to sign up for API access and obtain your API key. Make sure to carefully review and agree to any terms and conditions set by OpenAI.

4. Once you have your API key, you can proceed to configure it in the project as described in the next section.


## Configuration

1. Open the `.env` file.

2. Set the value of the `OPENAI_API_KEY` variable to your OpenAI API key. This key is required to authenticate your requests to the OpenAI API.

   ```
   OPENAI_API_KEY=YOUR_API_KEY
   ```

## Usage

1. Open a terminal or command prompt and navigate to the project directory.

2. Run the following command to execute the `make_function.py` script:

   ```
   python make_function.py -f <prompt_file> -l <language>
   ```

   - Replace `<prompt_file>` with the path to the file containing the prompt. Alternatively, use `-` to read the prompt from stdin.

   - Replace `<language>` with the desired language for the generated function (e.g., python).

   Example usage:

   ```
   python make_function.py -f prompt.txt -l python
   ```

   This will generate and print the function based on the provided prompt.

## Contributing

Contributions are welcome! If you find any issues or want to enhance this project, please submit an issue or a pull request.

## License

This project is licensed under the [MIT License](LICENSE).

## Acknowledgments

- [OpenAI](https://openai.com/) for providing the powerful GPT-3.5 Turbo model and the API.
- Manolo Guerrero for the Python packages used in this project.

Please note that this project is provided as-is without any warranty. Use it at your own risk.
